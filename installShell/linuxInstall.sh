#!/bin/bash
PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:~/bin
export PATH

# 检查root用户执行
if [ $(id -u) != "0" ]; then
    echo "Error: 该脚本执行需要root权限, 请用root用户执行superMiner安装"
    exit 1
fi

if [ -f /bin/superminer ]; then
    echo "Error: 您已经安装superMiner，无需重复安装"
    echo -e "如果你想重新安装superMiner,请先卸载老版本，执行：."
    exit 1
fi

Ubuntu_Modify_Source()
{
    apt-get install -y git build-essential cmake libuv1-dev libmicrohttpd-dev

    if [ ! -f "~/superminerin" ];then
        mkdir ~/superminerin
    else
        rm -rf ~/superminerin
        mkdir ~/superminerin
    fi

    git clone git@bitbucket.org:wangml/superminerinstall.git ~/superminerin/

    cd ~/superminer

    mkdir build

    cd build

    cmake ..

    make
}

# 判断系统，目前判断Ubuntu和CentOS

if grep -Eqi "CentOS" /etc/issue || grep -Eq "CentOS" /etc/*-release; then
    DISTRO='CentOS'
    PM='yum'
elif grep -Eqi "Ubuntu" /etc/issue || grep -Eq "Ubuntu" /etc/*-release; then
    DISTRO='Ubuntu'
    PM='apt'
else
    DISTRO='unknow'
fi

if [ "${DISTRO}" = "CentOS" ]; then
    Centos_Modify_Source
fi
if [ "${DISTRO}" = "Ubuntu" ]; then
    Ubuntu_Modify_Source
fi






# #lauch xmrig
# ./xmrig -o 35.196.87.177:8888 -u arifarfx -p x -k --av=2 --donate-level=1